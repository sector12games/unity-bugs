﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(SimpleScript))]
public class SimpleScriptEditor : Editor {

	public override void OnInspectorGUI()
	{
		SimpleScript script = (SimpleScript)target;
		script.timer = EditorGUILayout.FloatField("Timer: ", script.timer);
	}
}
