1) What happened

When using a custom inspector, if you lock the inspector window and then press "play", the inspector's values become stuck, and do not update. The same happens if you press play, then lock the inspector window - when you press "stop", the inspector becomes stuck and does not reset its values.

This only happens when using a custom inspector.

2) How can we reproduce it using the example you attached

1. Click on the "broken" game object I've created. 
2. Lock the inspector.
3. Press play.
4. Notice that the "timer" variable does not update.
5. While still playing, unlock the inspector.
6. Unselect the broken game object (by clicking on the main camera).
7 Re-select the broken game object.
8. Notice that it is now updating correctly.

The same is possible in reverse:

1. Make sure the inspector is NOT locked.
2. Press play.
3. Select the "broken" game object.
4. Notice that the "timer" variable is updating correctly.
5. Lock the inspector.
6. Notice that the "timer" variable is still updating correctly.
7. Press stop.
8. Notice that the "timer" variable has not reset back to 0, as should be expected.
9. If you unlock the inspector, deselect, and reselect the game object, the value will be set to 0.

This seems to be a problem with the inspector redrawing, and not a problem with the actual data being set. 

I've created a second, working game object, that has the same code but does not use a custom inspector. It works as intended, so you can compare the two if desired.